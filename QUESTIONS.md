# Answers

**Qs1:** '2' will be logged to the console then, a minimum of 100ms later, '1' will be logged to the console.

setTimeout starts a timer that executes the supplied function in the next event cycle after the time elapses, without halting the flow of execution, therefore the second log() call executes before the first.

**Qs2:** foo(0) will log the integers from 10 to 0 in descending order.

When d < 10, foo() calls itself recursively with d + 1, before logging d. The log() statement will not execute until that call resolves, so when called with an initial value of 0 we create a stack of 11 function calls that unwind in reverse order:

```
  foo(0)
    -> foo(1)
      -> foo(2)
        …
          -> foo(10)
          console.log(10)
        …
      console.log(2)
    console.log(1)
  console.log(0)
```

**Qs3:** Any falsy value, e.g. 0 or the empty string, passed into the function will be defaulted to 5.

i.e. foo(0), foo("") and foo() will all produce the same output, despite only the last of these meeting the requirement of “nothing is provided”.

Better, less error-prone approaches, pre-/post-ES2015:

```js
function foo(d) {
  d = typeof d !== "undefined" ? d : 5;
  console.log(d);
}

function foo(d = 5) {
  console.log(d);
}
```

**Qs4:** The code will output '3'.

foo(1) is a higher-order function returning a closure. In the scope of the anonymous function, the variable a remains bound to the value 1 that was passed to foo(), so bar is simply a reference to a function that adds the parameter b to 1.

Converting a function that accepts multiple parameters into a chain of functions that each take a single parameter is called currying.

**Qs5:** double() is a higher-order function that takes a function as its second parameter. After 100ms, the first parameter will be multiplied by 2 and passed that function.

This mimics an asynchronous callback, typically used when some function needs to operate on the result of a potentially long-running task.

e.g.

```js
// After 100ms, create a dialog with the content '4'
double(2, alert);

// After 100ms, log '6' to the console
double(3, console.log);
```

# Questions

**Qs1:** Explain the output of the following code and why

```js
setTimeout(function () {
  console.log("1");
}, 100);
console.log("2");
```

**Qs2:** Explain the output of the following code and why

```js
function foo(d) {
  if (d < 10) {
    foo(d + 1);
  }
  console.log(d);
}
foo(0);
```

**Qs3:** If nothing is provided to `foo` we want the default response to be `5`. Explain the potential issue with the following code:

```js
function foo(d) {
  d = d || 5;
  console.log(d);
}
```

**Qs4:** Explain the output of the following code and why

```js
function foo(a) {
  return function (b) {
    return a + b;
  };
}
var bar = foo(1);
console.log(bar(2));
```

**Qs5:** Explain how the following function would be used

```js
function double(a, done) {
  setTimeout(function () {
    done(a * 2);
  }, 100);
}
```
