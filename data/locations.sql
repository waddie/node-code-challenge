CREATE TABLE locations(
  geonameid INT PRIMARY KEY ASC,
  name TEXT NOT NULL,
  asciiname TEXT NOT NULL,
  alternatenames TEXT,
  latitude REAL,
  longitude REAL,
  feature_class TEXT,
  feature_code TEXT,
  country_code TEXT,
  cc2 TEXT,
  admin1_code TEXT,
  admin2_code TEXT,
  admin3_code TEXT,
  admin4_code TEXT,
  population INT,
  elevation REAL,
  dem INT,
  timezone TEXT,
  modification_date TEXT
);

CREATE INDEX idxName ON locations(name);
CREATE INDEX idxAsciiName ON locations(asciiname);
CREATE INDEX idxAlternateNames ON locations(alternatenames);
