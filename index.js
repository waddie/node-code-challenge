import sqlite3 from "sqlite3";
import { open } from "sqlite";
import express from "express";

import {
  locationsQuery,
  suggestQuery,
  totalQuery,
  fetchQuery,
} from "./src/server/sql.js";

import { matchingName } from "./src/server/util.js";

const suggestionsLimit = 10,
  resultsLimit = 10,
  minimumQueryLength = 2;

let closing = false; // killing npm start triggers SIGINT twice

(async () => {
  const app = express(),
    port = 1700,
    db = await open({
      filename: "./data/geo.db",
      driver: sqlite3.cached.Database,
    });

  /**
   * Prepare sqlite queries used by the API endpoints
   */
  const fetch = await db.prepare(locationsQuery),
    fetchPaginated = await db.prepare(fetchQuery),
    total = await db.prepare(totalQuery),
    suggest = await db.prepare(suggestQuery);

  /**
   * Service /public folder as static content
   */
  app.use(express.static("public"));

  /**
   * API endpoint for /locations?q=matchString
   */
  app.get("/locations", async (req, res) => {
    res.setHeader("Content-Type", "application/json; charset=utf-8");
    res.setHeader("Access-Control-Allow-Origin", "*");

    if (
      typeof req.query.q === "string" &&
      req.query.q.length >= minimumQueryLength
    ) {
      const results = await fetch.all({
        "@startsWith": req.query.q + "%",
        "@separated": "%," + req.query.q + "%",
      });

      res.send(JSON.stringify(results.map((row) => row.name)) + "\n");
    } else {
      res.send("{}\n");
    }
  });

  /**
   * Better endpoint to retrieve results with pagination
   */
  app.get("/locations/:query/:offset?", async (req, res) => {
    res.setHeader("Content-Type", "application/json; charset=utf-8");
    res.setHeader("Access-Control-Allow-Origin", "*");

    if (
      typeof req.params.query === "string" &&
      req.params.query.length >= minimumQueryLength
    ) {
      // Offset is optional, default to 0 if we don't have an integer
      const offset =
        typeof req.params.offset === "string" &&
        req.params.offset.length > 0 &&
        Number.isInteger(parseInt(req.params.offset))
          ? parseInt(req.params.offset)
          : 0;

      // Retrieve total matching results, and this batch of results, and combine them
      const count = await total.get({
          "@startsWith": req.params.query + "%",
          "@separated": "%," + req.params.query + "%",
        }),
        results = await fetchPaginated.all({
          "@startsWith": req.params.query + "%",
          "@separated": "%," + req.params.query + "%",
          "@limit": resultsLimit,
          "@offset": offset,
          "@latitude": req.query.latitude,
          "@longitude": req.query.longitude,
        }),
        response = {
          total: count.count,
          offset: offset,
          query: req.params.query,
          results: results,
        };

      res.send(JSON.stringify(response) + "\n");
    } else {
      res.send("{}\n");
    }
  });

  /**
   * Suggestions endpoint, array of matching names from any field
   */
  app.get("/suggest/:query", async (req, res) => {
    res.setHeader("Content-Type", "application/json; charset=utf-8");
    res.setHeader("Access-Control-Allow-Origin", "*");

    if (
      typeof req.params.query === "string" &&
      req.params.query.length >= minimumQueryLength
    ) {
      const results = await suggest.all({
        "@startsWith": req.params.query + "%",
        "@separated": "%," + req.params.query + "%",
      });

      /**
       * There may be hundreds of matches, in any of 3 fields, including exact duplicates.
       *
       * So:
       *
       * • Return only the matching name, from name, asciiname or the alternates.
       * • Sort them shortest first.
       * • Reduce the list to the first <suggestionsLimit> results.
       *
       * If we were using a remote DB, this would be better handled in SQL.
       */
      const suggestions = [
        ...new Set(
          results
            .map((row) => matchingName(row, req.params.query))
            .sort((a, b) => a.length - b.length)
        ),
      ].slice(0, suggestionsLimit);

      res.send(JSON.stringify(suggestions) + "\n");
    } else {
      res.send("[]\n");
    }
  });

  /**
   * Start the server
   */
  app.listen(port, async () => {
    console.info(`Geo search app listening at http://localhost:${port}`);
  });

  /**
   * Close the database before quitting
   */
  async function closeConnections() {
    if (!closing) {
      console.info("\nClosing connection…");

      closing = true;

      try {
        await fetch.finalize();
        await suggest.finalize();
        await total.finalize();
        await fetchPaginated.finalize();
        await db.close();
      } catch (e) {
        console.warn(e.message);
      } finally {
        process.exit();
      }
    }
  }

  process.on("SIGINT", closeConnections);
})();
