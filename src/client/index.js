import React from "react";
import { render } from "react-dom";
import GeoSearch from "./search-form";

document.addEventListener("DOMContentLoaded", () => {
  const searchForm = document.querySelector("#searchForm");

  searchForm && render(<GeoSearch />, searchForm);
});
