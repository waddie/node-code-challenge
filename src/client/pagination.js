import React from "react";
import PropTypes from "prop-types";

/**
 * Renders standard pagination controls patttern for the results.
 *
 * @param {object} props React props.
 * @returns {object} - React component.
 */
function Pagination(props) {
  const numberOfPages = Math.ceil(props.total / props.pageSize),
    currentPage = Math.ceil((props.offset + 1) / props.pageSize),
    pages = [];

  pages.push(
    <button
      className="pagination__controls__button--prev"
      key="prev"
      type="button"
      disabled={currentPage === 1 ? true : false}
      onClick={() => {
        props.submitForm(props.query, props.offset - props.pageSize);
      }}
    >
      <span>Previous page</span>
    </button>
  );

  for (let page = 1; page <= numberOfPages; page++) {
    let className;
    switch (page - numberOfPages) {
      case 0:
        className =
          "pagination__controls__element pagination__controls__button pagination__controls__button--last";
        break;
      case -1:
        className =
          "pagination__controls__element pagination__controls__button pagination__controls__button--penultimate";
        break;
      default:
        className =
          "pagination__controls__element pagination__controls__button";
    }

    pages.push(
      <button
        aria-current={page === currentPage ? "page" : null}
        aria-expanded={page === currentPage ? true : false}
        aria-label={`Open page ${page}`}
        className={className}
        data-page={page}
        data-proximity={Math.abs(page - currentPage)}
        disabled={page === currentPage ? true : false}
        key={page}
        type="button"
        onClick={() => {
          props.submitForm(props.query, (page - 1) * props.pageSize);
        }}
      >
        <span>{page}</span>
      </button>
    );

    if (page === 1) {
      pages.push(
        <span
          key="firstEllipsis"
          className="pagination__controls__element pagination__controls__ellipsis pagination__controls__ellipsis--first"
        >
          <span>…</span>
        </span>
      );
    } else if (page === numberOfPages - 1) {
      pages.push(
        <span
          key="lastEllipsis"
          className="pagination__controls__element pagination__controls__ellipsis pagination__controls__ellipsis--last"
        >
          <span>…</span>
        </span>
      );
    }
  }

  pages.push(
    <button
      className="pagination__controls__button--next"
      key="next"
      type="button"
      disabled={currentPage === numberOfPages ? true : false}
      onClick={() => {
        props.submitForm(props.query, props.offset + props.pageSize);
      }}
    >
      <span>Next page</span>
    </button>
  );

  return (
    <div className="pagination__wrapper">
      <nav className="pagination__controls" data-pagecount={numberOfPages}>
        {pages}
      </nav>
    </div>
  );
}

Pagination.propTypes = {
  offset: PropTypes.number,
  pageSize: PropTypes.number,
  query: PropTypes.string,
  total: PropTypes.number,
  submitForm: PropTypes.func,
};

export default Pagination;
