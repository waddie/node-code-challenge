import { getDistance } from "geolib";
import PropTypes from "prop-types";
import React from "react";

const metresAccuracy = 250,
  kilometre = 1000,
  apiKey = "AIzaSyCBWhpWybzY-P9yTRBJ8UgjsP45QejiLOs";

function Results(props) {
  return (
    <div>
      <p aria-live="polite">
        Showing <b>{props.results.offset + 1}</b> to{" "}
        <b>{props.results.results.length + props.results.offset}</b> of{" "}
        <b>{props.results.total}</b>{" "}
        {props.results.total > 1 ? "results" : "result"}, nearest to{" "}
        {props.coords.location} first.
      </p>
      <ol start={props.results.offset + 1} className="searchForm__results">
        {props.results.results.map((result, i) => {
          return (
            <li key={i}>
              <h2>{result.name}</h2>
              <div className="searchForm__results__result-content">
                <div>
                  <img
                    src={`https://maps.google.com/maps/api/staticmap?key=${apiKey}&amp;zoom=13&size=200x200&scale=2&markers=color:red|${result.latitude},${result.longitude}`}
                    width="200"
                    height="200"
                    alt="Map showing {result.name}"
                  />
                </div>
                <div>
                  {result.alternatenames &&
                    result.alternatenames !== result.name && (
                      <p>
                        <b>Alternate names:</b>{" "}
                        {result.alternatenames.split(/,/g).join(", ")}
                      </p>
                    )}
                  <p>
                    <b>Distance:</b> Approximately{" "}
                    {getDistance(
                      {
                        latitude: result.latitude,
                        longitude: result.longitude,
                      },
                      {
                        latitude: props.coords.latitude,
                        longitude: props.coords.longitude,
                      },
                      metresAccuracy
                    ) / kilometre}
                    km from {props.coords.location}
                  </p>
                </div>
              </div>
            </li>
          );
        })}
      </ol>
    </div>
  );
}

Results.propTypes = {
  results: PropTypes.arrayOf(PropTypes.object),
  coords: PropTypes.object,
};

export default Results;
