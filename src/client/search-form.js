import PropTypes from "prop-types";
import React, { useEffect, useState } from "react";

import Pagination from "./pagination";
import Suggestions from "./suggestions";
import Results from "./results";

import { replaceHistory, suggest, fetch, getCoords } from "./util";

/**
 * TODO: Move into config JSON loaded on launch
 */
const minimumQueryLength = 2,
  defaultResults = { total: 0, offset: 0, query: null, results: [] },
  defaultLocation = {
    location: "the MedicSpot offices",
    latitude: 51.4963356,
    longitude: -0.1473523,
  },
  pageSize = 10;

function Search() {
  const [suggestions, setSuggestions] = useState([]),
    [showSuggestions, setShowSuggestions] = useState(false),
    [results, setResults] = useState(defaultResults),
    [suggestionsCall, setSuggestionsCall] = useState({ cancel: () => {} }),
    [resultsCall, setResultsCall] = useState({ cancel: () => {} }),
    [partialQuery, setPartialQuery] = useState(""),
    [coords, setCoords] = useState(defaultLocation);

  /**
   * Variable and function for focusing the input field
   */
  let textInput = null;
  function focusInput() {
    textInput.focus();
  }

  /**
   * Handle form submissions.
   *
   * @param {string} query
   * @param {integer} offset
   */
  const submitForm = (query = partialQuery, offset = 0) => {
    resultsCall.cancel();
    setSuggestions([]);
    setResults(defaultResults);
    replaceHistory(query, offset);

    if (query.length >= minimumQueryLength) {
      // input is populated, fetch results
      const [resultsPromise, newCall] = fetch(
        query,
        offset,
        coords.latitude,
        coords.longitude
      );

      // update our request cancel function for the new request
      setResultsCall({
        cancel: () => {
          newCall.cancel();
        },
      });

      resultsPromise
        .then((data) => setResults(data))
        .catch(() => setResults(defaultResults));
    } else {
      // input is empty/not long enough, empty suggestions
      setSuggestions([]);
    }
  };

  const submitSuggestion = (suggestion) => {
    setPartialQuery(suggestion);
    setShowSuggestions(false);
    setSuggestions([]);
    focusInput();
    submitForm(suggestion);
  };

  /**
   * On load, if we have query parameters, used them to search.
   *
   * Resolve geoloc coordinates if we have permission, overriding default location.
   */
  useEffect(() => {
    const params = new URLSearchParams(window.location.search),
      query = params.get("query") || "",
      offset = params.get("offset") || 0;

    setPartialQuery(query);

    navigator.geolocation.getCurrentPosition(() =>
      getCoords().then((coords) => {
        setCoords(coords);
        submitForm(query, offset);
      })
    );

    query && submitForm(query, offset);
  }, []);

  const suggestionsRendered =
    suggestions && suggestions.length > 0 ? (
      <Suggestions
        suggestions={suggestions}
        partialQuery={partialQuery}
        submitSuggestion={submitSuggestion}
        setSuggestions={setSuggestions}
        showSuggestions={showSuggestions}
        setShowSuggestions={setShowSuggestions}
      />
    ) : null;

  const resultsRendered =
    results && results.results && results.results.length > 0 ? (
      <Results results={results} coords={coords} />
    ) : results.query !== null ? (
      <div>
        <p aria-live="polite">No results.</p>
      </div>
    ) : null;

  const pagination =
    results.total > pageSize ? (
      <Pagination
        offset={results.offset}
        pageSize={pageSize}
        query={results.query}
        total={results.total}
        submitForm={submitForm}
      />
    ) : null;

  return (
    <form
      onSubmit={(e) => {
        e.preventDefault();
      }}
    >
      <fieldset>
        <label htmlFor="query">Search</label>
        <div className="wrapper--searchForm__input">
          <input
            id="query"
            name="query"
            value={partialQuery}
            placeholder="e.g. Hastings"
            ref={(input) => {
              textInput = input;
            }}
            onFocus={() => {
              setShowSuggestions(true);
            }}
            onBlur={() => setShowSuggestions(false)}
            onChange={(e) => {
              //clear old suggestions
              setSuggestions([]);
              // keep  what they're typing
              setPartialQuery(e.target.value);

              if (e.target.value.length >= minimumQueryLength) {
                /**
                 * if we have a request to the suggestions endpoint in progress,
                 * cancel it. Stops old suggestions overwriting new ones because
                 * the requests can't keep up with fast typing.
                 */
                suggestionsCall.cancel();

                if (e.target.value) {
                  // input is populated, ask for suggestions
                  const [suggestionsPromise, newCall] = suggest(e.target.value);

                  // update our request cancel function for the new request
                  setSuggestionsCall({
                    cancel: () => {
                      newCall.cancel();
                    },
                  });

                  suggestionsPromise
                    .then((data) => setSuggestions(data))
                    .catch(() => setSuggestions([]));
                } else {
                  // input is empty/not long enough, empty suggestions
                  setSuggestions([]);
                }
              }
            }}
            onKeyDown={(e) => {
              switch (e.key) {
                case "Escape":
                  // clear the query
                  setPartialQuery("");
                  break;
                case "ArrowDown":
                  // move into the suggestions if they’re present
                  if (suggestions && suggestions.length > 0) {
                    e.preventDefault();
                    e.target.parentNode.parentNode
                      .querySelector(".searchForm__suggestions button")
                      .focus();
                  }
                  break;
                default:
                  break;
              }
            }}
          />
          <button type="submit" onClick={() => submitForm()}>
            Submit
          </button>
        </div>
        {suggestionsRendered}
      </fieldset>
      {resultsRendered}
      {pagination}
    </form>
  );
}

Search.propTypes = {
  element: PropTypes.object,
};

export default Search;
