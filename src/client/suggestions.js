import PropTypes from "prop-types";
import React from "react";
import { highlightQueryTerm } from "./util";

function Suggestions(props) {
  return (
    <ul
      role="listbox"
      aria-label="Search suggestions"
      className="searchForm__suggestions"
      data-hide={!props.showSuggestions}
    >
      {props.suggestions.map((suggestion, i) => {
        return (
          <li key={i} role="option">
            <button
              type="button"
              onMouseDown={() => props.submitSuggestion(suggestion)}
              onClick={() => props.submitSuggestion(suggestion)}
              onBlur={() => {
                props.setShowSuggestions(false);
              }}
              onFocus={() => props.setShowSuggestions(true)}
              onKeyDown={(e) => {
                switch (e.key) {
                  case "Escape":
                    e.target.parentNode.parentNode.parentNode
                      .querySelector("input")
                      .focus();
                    props.setSuggestions([]);
                    break;
                  case "ArrowUp":
                    if (
                      e.target.parentNode.previousElementSibling &&
                      e.target.parentNode.previousElementSibling.querySelector(
                        "button"
                      )
                    ) {
                      e.preventDefault();
                      e.target.parentNode.previousElementSibling
                        .querySelector("button")
                        .focus();
                    } else {
                      e.preventDefault();
                      e.target.parentNode.parentNode.parentNode
                        .querySelector("input")
                        .focus();
                    }
                    break;
                  case "ArrowDown":
                    e.preventDefault();
                    if (
                      e.target.parentNode.nextElementSibling &&
                      e.target.parentNode.nextElementSibling.querySelector(
                        "button"
                      )
                    ) {
                      e.preventDefault();
                      e.target.parentNode.nextElementSibling
                        .querySelector("button")
                        .focus();
                    }
                    break;
                  case "Home":
                    e.preventDefault();
                    if (
                      e.target.parentNode.parentNode.firstChild &&
                      e.target.parentNode.parentNode.firstChild.querySelector(
                        "button"
                      )
                    ) {
                      e.preventDefault();
                      e.target.parentNode.parentNode.firstChild
                        .querySelector("button")
                        .focus();
                    }
                    break;
                  case "End":
                    e.preventDefault();
                    if (
                      e.target.parentNode.parentNode.lastChild &&
                      e.target.parentNode.parentNode.lastChild.querySelector(
                        "button"
                      )
                    ) {
                      e.preventDefault();
                      e.target.parentNode.parentNode.lastChild
                        .querySelector("button")
                        .focus();
                    }
                    break;
                  default:
                    break;
                }
              }}
            >
              {highlightQueryTerm(suggestion, props.partialQuery)}
            </button>
          </li>
        );
      })}
    </ul>
  );
}

Suggestions.propTypes = {
  suggestions: PropTypes.arrayOf(PropTypes.string),
  partialQuery: PropTypes.string,
  submitSuggestion: PropTypes.func,
  setSuggestions: PropTypes.func,
  showSuggestions: PropTypes.bool,
  setShowSuggestions: PropTypes.func,
};

export default Suggestions;
