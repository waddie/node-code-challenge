import React from "react";

const axios = require("axios");

const timeout = 5000,
  port = 1700;

/**
 * lvh.me resolves to localhost
 *
 * This is just to get round CORS issues for the purposes of a demo.
 */
const rootUrl = "http://lvh.me";

/**
 * Preserve the search state in the URL parameters.
 *
 * @param {string} query The search query.
 * @param {integer} offest The offset number.
 */
export function replaceHistory(query, offset) {
  if (window) {
    const params = new URLSearchParams(window.location.search);

    // Preserve params only if they’re not the defaults
    query !== "" ? params.set("query", query) : params.delete("query");
    offset !== 0 ? params.set("offset", offset) : params.delete("offset");

    const hasParams = params.toString().length ? "?" : "";

    window.history.replaceState(
      {},
      "",
      `${window.location.pathname}${hasParams}${params.toString()}`
    );
  }
}

/**
 * Asynchronous http request.
 *
 * @param {object} config An axios configuration object.
 * @returns {Promise} - A promise of a Funnelback response.
 */
function axiosRequest(config) {
  const httpOK = 200;

  return axios(config)
    .then((response) => {
      if (httpOK === response.status) {
        return response.data;
      } else {
        throw `Bad response: ${response.status}`;
      }
    })
    .catch((e) => {
      if (!axios.isCancel(e)) {
        console.log(`Line ${e.lineNumber} – ${e.message}`);
      }
    });
}

/**
 * Fetch suggestions.
 *
 * @param {string} partialQuery The string to make suggestions for.
 * @returns {Promise} - A promise of an array of suggestion strings.
 */
export function suggest(partialQuery) {
  const CancelToken = axios.CancelToken,
    call = CancelToken.source(),
    config = {
      baseURL: rootUrl + ":" + port,
      cancelToken: call.token,
      url: "/suggest/" + encodeURI(partialQuery),
      timeout: timeout,
      params: {},
    };

  return [axiosRequest(config), call];
}

/**
 * Fetch results.
 *
 * @param  {string} query The string to make suggestions for.
 * @param  {integer} offset The results offset.
 * @param  {number} latitude The user’s latitude.
 * @param  {number} longitude The user’s longitude.
 * @returns {Promise} - A promise of a results object.
 */
export function fetch(query, offset, latitude, longitude) {
  const CancelToken = axios.CancelToken,
    call = CancelToken.source(),
    config = {
      baseURL: rootUrl + ":" + port,
      cancelToken: call.token,
      url: "/locations/" + encodeURI(query) + "/" + offset,
      timeout: timeout,
      params: {
        latitude: latitude,
        longitude: longitude,
      },
    };

  return [axiosRequest(config), call];
}

/**
 * Make the matching part of the string bold.
 *
 * @param  {string} suggestion
 * @param  {string} partialQuery
 */
export function highlightQueryTerm(suggestion, partialQuery) {
  return (
    <React.Fragment>
      <strong>{suggestion.slice(0, partialQuery.length)}</strong>
      {suggestion.slice(partialQuery.length)}
    </React.Fragment>
  );
}

/**
 * Get the browser location if permission is granted, else default to MedicSpot offices
 *
 * @param {object} defaultLocation Coordinates for MedicSpot head office.
 */
export function getCoords(defaultLocation) {
  return new Promise((resolve, reject) =>
    navigator.permissions
      ? navigator.permissions
          .query({
            name: "geolocation",
          })
          .then((permission) =>
            permission.state === "granted"
              ? navigator.geolocation.getCurrentPosition((pos) =>
                  resolve({
                    location: "your location",
                    latitude: pos.coords.latitude,
                    longitude: pos.coords.longitude,
                  })
                )
              : resolve(defaultLocation)
          )
      : reject(defaultLocation)
  );
}
