/**
 * Return the bare minimum to satisfy inital requirement.
 */
export const locationsQuery = `SELECT name, asciiname, alternatenames
                               FROM locations
                               WHERE name LIKE @startsWith OR
                                     asciiname LIKE @startsWith OR
                                     alternatenames LIKE @startsWith OR
                                     alternatenames LIKE @separated
                               ORDER BY length(name) ASC`;

/**
 * Return total matches for a search term.
 */
export const totalQuery = `SELECT count(*) count
                           FROM locations
                           WHERE name LIKE @startsWith OR
                                 asciiname LIKE @startsWith OR
                                 alternatenames LIKE @startsWith OR
                                 alternatenames LIKE @separated`;

/**
 * Return results for a search term paginated and sorted by nearest location.
 *
 * TODO: sqlite could be built with SpatiaLite for accurate distance calculation, this is a cheap hack.
 */
export const fetchQuery = `SELECT geonameid, name, asciiname, alternatenames, latitude, longitude
                           FROM locations
                           WHERE name LIKE @startsWith OR
                                 asciiname LIKE @startsWith OR
                                 alternatenames LIKE @startsWith OR
                                 alternatenames LIKE @separated
                           ORDER BY ((latitude - @latitude) * (latitude - @latitude)) + ((longitude - @longitude) * (longitude - @longitude)) ASC
                           LIMIT @offset, @limit`;

/**
 * Return just the matching names for a search term.
 */
export const suggestQuery = `SELECT name, asciiname, alternatenames
                             FROM locations
                             WHERE name LIKE @startsWith OR
                                   asciiname LIKE @startsWith OR
                                   alternatenames LIKE @startsWith OR
                                   alternatenames LIKE @separated`;
