/**
 * Predicate returns true if (lowercased) potentialMatch starts with the
 * (lowercased)  query.
 *
 * @param  {string} potentialMatch
 * @param  {string} query
 * @returns {boolean}
 */
const matchesQuery = (potentialMatch, query) =>
  potentialMatch.toLowerCase().indexOf(query.toLowerCase()) === 0;

/**
 * Return the first matching of the various alternate names from a result.
 *
 * @param  {object} row
 * @param  {string} query
 * @returns {string} - The first matching name.
 */
export function matchingName(row, query) {
  return matchesQuery(row.name, query)
    ? row.name
    : matchesQuery(row.asciiname, query)
    ? row.asciiname
    : row.alternatenames.split(/,/g).find((name) => matchesQuery(name, query));
}
