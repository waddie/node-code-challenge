import { matchingName } from "./util";

const testRow = {
  name: "Agía Theodora",
  asciiname: "Agia Theodora",
  alternatenames: "Agía Theodóra,Ayia Theodhora,Αγία Θεοδώρα",
};

test("matches Agía Theodora in name", () => {
  expect(matchingName(testRow, "Ag")).toBe("Agía Theodora");
});

test("matches Agia Theodora in asciiname", () => {
  expect(matchingName(testRow, "Agi")).toBe("Agia Theodora");
});

test("matches Αγία Θεοδώρα in alternatenames", () => {
  expect(matchingName(testRow, "Αγ")).toBe("Αγία Θεοδώρα");
});
