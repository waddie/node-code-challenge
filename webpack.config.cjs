const path = require("path"),
  minifyPlugin = require("babel-minify-webpack-plugin");

module.exports = {
  entry: "./src/client/index.js",
  devtool: "source-map",
  output: {
    filename: "index.js",
    path: path.resolve(__dirname, "public"),
  },
  module: {
    rules: [
      {
        test: /\.m?js$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
          options: {
            presets: [
              [
                "@babel/preset-env",
                {
                  corejs: 3,
                  exclude: ["es.symbol.description"],
                  useBuiltIns: "usage",
                },
              ],
              "@babel/preset-react",
            ],
            plugins: [
              ["@babel/plugin-proposal-decorators", { legacy: true }],
              ["@babel/plugin-proposal-class-properties", { loose: true }],
              "@babel/plugin-transform-strict-mode",
              "transform-regexp-constructors",
            ],
          },
        },
      },
    ],
  },
  plugins: [
    new minifyPlugin({ mangle: { topLevel: true } }, { comments: false }),
  ],
};
